require 'test_helper'

class PocketUsersControllerTest < ActionController::TestCase
  setup do
    @pocket_user = pocket_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pocket_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pocket_user" do
    assert_difference('PocketUser.count') do
      post :create, pocket_user: {  }
    end

    assert_redirected_to pocket_user_path(assigns(:pocket_user))
  end

  test "should show pocket_user" do
    get :show, id: @pocket_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pocket_user
    assert_response :success
  end

  test "should update pocket_user" do
    patch :update, id: @pocket_user, pocket_user: {  }
    assert_redirected_to pocket_user_path(assigns(:pocket_user))
  end

  test "should destroy pocket_user" do
    assert_difference('PocketUser.count', -1) do
      delete :destroy, id: @pocket_user
    end

    assert_redirected_to pocket_users_path
  end
end
