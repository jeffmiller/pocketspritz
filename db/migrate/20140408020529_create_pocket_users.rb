class CreatePocketUsers < ActiveRecord::Migration
  def change
    create_table :pocket_users do |t|
      t.string :pocket_user_id
      t.string :code
      t.string :username
      t.text :current_queue

      t.timestamps
    end
  end
end
