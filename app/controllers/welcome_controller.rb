require 'net/http'
require 'net/https'
require 'pp'

class WelcomeController < ApplicationController

  def index
    @auth_params = {
      :consumer_key => PocketSpritz::Application.config.pocket_web_api,
      :redirect_uri => 'http://localhost:3000/queue',
    }.to_json

    @headers = { 
      'Content-Type' => 'application/json; charset=UTF-8',
      'X-Accept'     => 'application/json', 
    }

    url = 'https://getpocket.com/v3/oauth/request'

    request = HTTPI::Request.new(url)
    request.headers = @headers
    request.body = @auth_params

    @code = JSON.parse(HTTPI.put(request).body)['code']
    @user = PocketUser.new
    @user.code = @code
    @user.save!

    session[:user_id] = @user.id
    url = "https://getpocket.com/auth/authorize?request_token=#{@code}&redirect_uri=http://localhost:3000/queue"
    redirect_to url
  end

  def test_spritz
  end

  def spritz_login_success
  end
end
