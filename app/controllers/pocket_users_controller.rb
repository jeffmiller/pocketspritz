require 'open-uri'

class PocketUsersController < ApplicationController
  before_action :set_pocket_user, only: [:show, :edit, :update, :destroy]

  # GET /pocket_users
  # GET /pocket_users.json
  def index
    @pocket_users = PocketUser.all
  end

  # GET /pocket_users/1
  # GET /pocket_users/1.json
  def show
  end

  # GET /pocket_users/new
  def new
    @pocket_user = PocketUser.new
  end

  # GET /pocket_users/1/edit
  def edit
  end

  # POST /pocket_users
  # POST /pocket_users.json
  def create
    @pocket_user = PocketUser.new(pocket_user_params)

    respond_to do |format|
      if @pocket_user.save
        format.html { redirect_to @pocket_user, notice: 'Pocket user was successfully created.' }
        format.json { render action: 'show', status: :created, location: @pocket_user }
      else
        format.html { render action: 'new' }
        format.json { render json: @pocket_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pocket_users/1
  # PATCH/PUT /pocket_users/1.json
  def update
    respond_to do |format|
      if @pocket_user.update(pocket_user_params)
        format.html { redirect_to @pocket_user, notice: 'Pocket user was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pocket_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pocket_users/1
  # DELETE /pocket_users/1.json
  def destroy
    @pocket_user.destroy
    respond_to do |format|
      format.html { redirect_to pocket_users_url }
      format.json { head :no_content }
    end
  end

  def queue
    if session[:user_id]
      @user = PocketUser.find(session[:user_id])

      url = 'https://getpocket.com/v3/oauth/authorize'
      request = HTTPI::Request.new(url)
      request.body = '{"consumer_key":"26064-2500754fd1a2ea5a05e2f6a0", "code":"CODE"}'
      request.body = request.body.gsub 'CODE', @user.code
      request.headers = {"Content-Type" => "application/json"}

      returned_vars = CGI.parse(HTTPI.post(request).body)
      @user.access_token = returned_vars['access_token'].first
      @user.username     = returned_vars['username'].first

      url = 'https://getpocket.com/v3/get'
      request = HTTPI::Request.new(url)
      request.body = {
        :consumer_key => PocketSpritz::Application.config.pocket_web_api,
        :access_token => @user.access_token,
      }.to_json
      request.body = '{"consumer_key":"26064-2500754fd1a2ea5a05e2f6a0", "access_token":"TOKEN", "count":"10"}'
      request.body = request.body.gsub 'TOKEN', @user.access_token
      request.headers = {"Content-Type" => "application/json"}

      @user.current_queue = HTTPI.post(request).body
      @user.save!

      @username = @user.username
      @articles = JSON.parse(@user.current_queue)
      @articles = @articles['list'].first 5
    else
      @username = "Could not find |#{session[:user_id]}|"
    end
  end

  def read
    if session[:user_id]
      @user = PocketUser.find(session[:user_id])
      @articles = JSON.parse(@user.current_queue)
      @articles = @articles['list']

      @articles.each do |article_id,article|
        if article_id.to_i == params[:id].to_i
=begin
          doc = Nokogiri::HTML(open(article['resolved_url'])) 
          all_p = doc.css('p')
          @article_body = all_p
          @article = article
=end
          @article_url = article['resolved_url']
          break
        else
          @article_body = "I did not find it...#{article_id}|#{params[:id].to_i}|#{article}"
          @article = @articles.size
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pocket_user
      @pocket_user = PocketUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pocket_user_params
      params[:pocket_user]
    end
end
