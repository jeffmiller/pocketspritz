json.array!(@pocket_users) do |pocket_user|
  json.extract! pocket_user, :id
  json.url pocket_user_url(pocket_user, format: :json)
end
